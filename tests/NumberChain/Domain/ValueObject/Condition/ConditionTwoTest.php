<?php

declare(strict_types=1);

namespace App\Tests\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\Condition\ConditionTwo;
use PHPUnit\Framework\TestCase;

class ConditionTwoTest extends TestCase
{
    public function testCanBeApplied(): void
    {
        $condition = ConditionTwo::create();
        self::assertTrue($condition->canBeApplied(1));
    }

    public function testCannotBeApplied(): void
    {
        $condition = ConditionTwo::create();
        self::assertFalse($condition->canBeApplied(0));
        self::assertFalse($condition->canBeApplied(5));
        self::assertFalse($condition->canBeApplied(20));
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\Condition\ConditionThree;
use PHPUnit\Framework\TestCase;

class ConditionThreeTest extends TestCase
{
    public function testCanBeApplied(): void
    {
        $condition = ConditionThree::create();
        self::assertTrue($condition->canBeApplied(2));
        self::assertTrue($condition->canBeApplied(10));
        self::assertTrue($condition->canBeApplied(200));
    }

    public function testCannotBeApplied(): void
    {
        $condition = ConditionThree::create();
        self::assertFalse($condition->canBeApplied(1));
        self::assertFalse($condition->canBeApplied(3));
        self::assertFalse($condition->canBeApplied(21));
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\Condition\ConditionOne;
use PHPUnit\Framework\TestCase;

class ConditionOneTest extends TestCase
{
    public function testCanBeApplied(): void
    {
        $condition = ConditionOne::create();
        self::assertTrue($condition->canBeApplied(0));
    }

    public function testCannotBeApplied(): void
    {
        $condition = ConditionOne::create();
        self::assertFalse($condition->canBeApplied(1));
        self::assertFalse($condition->canBeApplied(2));
        self::assertFalse($condition->canBeApplied(10));
    }
}

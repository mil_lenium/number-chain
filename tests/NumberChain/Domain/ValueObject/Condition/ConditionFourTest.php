<?php

declare(strict_types=1);

namespace App\Tests\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\Condition\ConditionFour;
use App\NumberChain\Domain\ValueObject\Condition\ConditionThree;
use PHPUnit\Framework\TestCase;

class ConditionFourTest extends TestCase
{
    public function testCanBeApplied(): void
    {
        $condition = ConditionFour::create();
        self::assertTrue($condition->canBeApplied(3));
        self::assertTrue($condition->canBeApplied(5));
        self::assertTrue($condition->canBeApplied(21));
    }

    public function testCannotBeApplied(): void
    {
        $condition = ConditionFour::create();
        self::assertFalse($condition->canBeApplied(1));
        self::assertFalse($condition->canBeApplied(2));
        self::assertFalse($condition->canBeApplied(20));
    }
}

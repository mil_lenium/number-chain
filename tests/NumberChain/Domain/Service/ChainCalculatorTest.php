<?php

declare(strict_types=1);

namespace App\Tests\NumberChain\Domain\Service;

use App\NumberChain\Domain\Service\DefaultChainCalculator;
use App\NumberChain\Domain\Service\DefaultConditionConfigProvider;
use App\NumberChain\Domain\Service\DefaultNumberCalculator;
use PHPUnit\Framework\TestCase;

class ChainCalculatorTest extends TestCase
{

    /**
     * @dataProvider numberProvider
     */
    public function testCalculate(array $numbers, array $expectedResult): void
    {
        $calculator = new DefaultChainCalculator($this->createCalculator());
        $result = $calculator->calculate($numbers);
        
        self::assertSame($expectedResult, $result);
    }

    public function numberProvider(): array
    {
        return [
            [[0, 1, 3, 5, 10], [0 => 0, 1 => 1, 3 => 2, 5 => 3, 10 => 4]],
            [[777, 7777, 77777], [777 => 89, 7777 => 377, 77777 => 2207]],
            [[9999, 99999], [9999 => 521, 99999 => 2584]]
        ];
    }

    private function createCalculator(): DefaultNumberCalculator
    {
        $configProvider = new DefaultConditionConfigProvider();
        return new DefaultNumberCalculator($configProvider->provide());
    }
}

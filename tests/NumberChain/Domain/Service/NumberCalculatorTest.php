<?php

declare(strict_types=1);

namespace App\Tests\NumberChain\Domain\Service;

use App\NumberChain\Domain\Service\DefaultConditionConfigProvider;
use App\NumberChain\Domain\Service\DefaultNumberCalculator;
use PHPUnit\Framework\TestCase;

class NumberCalculatorTest extends TestCase
{
    /**
     * @dataProvider numberProvider
     */
    public function testCalculate(int $number, int $expectedResult): void
    {
        $calculator = $this->createCalculator();
        $result = $calculator->calculate($number);

        self::assertSame($expectedResult, $result);
    }

    public function numberProvider(): array
    {
        return [
            [0, 0],
            [1, 1],
            [5, 3],
            [9, 4],
            [37, 11],
            [20000, 43],
            [77777, 449],
            [100000, 127]
        ];
    }

    private function createCalculator(): DefaultNumberCalculator
    {
        $configProvider = new DefaultConditionConfigProvider();
        return new DefaultNumberCalculator($configProvider->provide());
    }
}

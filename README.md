# number-chain

How to run project:

 - `cp .env.dist .env`
 - `composer install --ignore-platform-reqs`
 - `docker-compose up -d`
  
Command: `bin/console number:chain:calculate n n2 n3 n10`

 - Numbers are separated by spaces
 - Max 10 numbers
 
Endpoint: `/`

 - Contains form for numbers input
 - One number in one row
 - Max 10 numbers
 - After form submitting, numbers are calculated and results are printed in below table
 
Description:

 - Single number calculation is placed in `App\NumberChain\Domain\Service\DefaultNumberCalculator:calculate`
 - Conditions are separated to `simple` and `complex` eg. A1 = 1 is a simple one, A2n = An is a complex one.
 - Conditions are placed in `App\NumberChain\Domain\Condition`.
 - Project contains unit tests for conditions and calculators

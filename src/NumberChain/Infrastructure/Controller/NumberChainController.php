<?php

declare(strict_types=1);

namespace App\NumberChain\Infrastructure\Controller;

use App\NumberChain\Infrastructure\Processor\CalculateNumbersProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class NumberChainController extends AbstractController
{

    /**
     * @var CalculateNumbersProcessor
     */
    private $processor;

    public function __construct(CalculateNumbersProcessor $processor)
    {
        $this->processor = $processor;
    }

    public function index(Request $request)
    {
        $results = null;
        $form = $this->createInputForm();

        if ('POST' === $request->getMethod()) {
            $results = $this->calculateResult($request, $form);
        }

        return $this->render('number_chain.html.twig', ['form' => $form->createView(), 'results' => $results]);
    }

    private function createInputForm(): FormInterface
    {
        return $this->createFormBuilder()
            ->add('numbers', TextareaType::class, [
                'attr' => ['cols' => '10', 'rows' => '10']
            ])
            ->add('calculate', SubmitType::class, [
                'attr' => ['class' => 'btn-warning']
            ])
            ->getForm();
    }

    private function calculateResult(Request $request, FormInterface $form): ?array
    {
        $numbers = $request->request->get('form')['numbers'];
        $numbers = explode("\n", $numbers);

        if (10 < count($numbers)) {
            $form->addError(new FormError('More than 10 numbers provided.'));
            return null;
        }

        return $this->processor->process($numbers);
    }
}

<?php

declare(strict_types=1);

namespace App\NumberChain\Infrastructure\Processor;

use App\NumberChain\Domain\Factory\ChainCalculatorFactory;
use App\NumberChain\Domain\Service\ChainCalculator;

class DefaultCalculateNumbersProcessor implements CalculateNumbersProcessor
{
    /**
     * @var ChainCalculator
     */
    private $calculator;

    public function __construct(ChainCalculatorFactory $calculatorFactory)
    {
        $this->calculator = $calculatorFactory->create();
    }

    public function process(array $numbers): array
    {
        return $this->calculator->calculate($numbers);
    }
}

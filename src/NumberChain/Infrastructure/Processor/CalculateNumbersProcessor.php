<?php

declare(strict_types=1);

namespace App\NumberChain\Infrastructure\Processor;

interface CalculateNumbersProcessor
{
    public function process(array $numbers): array;
}

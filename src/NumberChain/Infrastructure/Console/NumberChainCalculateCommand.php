<?php

declare(strict_types=1);

namespace App\NumberChain\Infrastructure\Console;

use App\NumberChain\Infrastructure\Processor\CalculateNumbersProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NumberChainCalculateCommand extends Command
{
    protected static $defaultName = 'number:chain:calculate';

    /**
     * @var CalculateNumbersProcessor
     */
    private $processor;

    public function __construct(CalculateNumbersProcessor $processor, string $name = null)
    {
        parent::__construct($name);
        $this->processor = $processor;
    }

    protected function configure()
    {
        parent::configure();
        $this->addArgument(
            'numbers',
            InputArgument::IS_ARRAY | InputArgument::REQUIRED,
            'Numbers (separate multiple names with a space)?'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $code = 0;
        $this->configureOutput($output);
        $numbers = $input->getArgument('numbers');


        if (10 < count($numbers)) {
            $output->writeln('<error>Only 10 numbers can be provided.</error>');
            $code = 1;
            return $code;
        }

        $result = $this->processor->process($numbers);
        $output->writeln("Results:");

        foreach ($result as $number => $maxValue) {
            $output->writeln("    <result>{$number} => {$maxValue}</result>");
        }

        return $code;
    }

    private function configureOutput(OutputInterface $output): void
    {
        $errorStyle = new OutputFormatterStyle('red', null, ['bold', 'blink']);
        $resultStyle = new OutputFormatterStyle('blue', null, ['bold', 'blink']);

        $output->getFormatter()->setStyle('error', $errorStyle);
        $output->getFormatter()->setStyle('result', $resultStyle);
    }
}

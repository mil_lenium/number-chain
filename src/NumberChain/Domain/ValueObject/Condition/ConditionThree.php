<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\ComplexCondition;
use App\NumberChain\Domain\ValueObject\NumberCollector;

final class ConditionThree implements ComplexCondition
{
    public static function create(): self
    {
        return new self();
    }

    public function apply(int $number): NumberCollector
    {
        $number = $number / 2;

        if ($this->canBeApplied($number)) {
            return $this->apply($number);
        }

        return NumberCollector::fromArray([$number]);
    }

    public function canBeApplied(int $number): bool
    {
        if (0 === $number) {
            return false;
        }

        return 0 === $number % 2;
    }
}

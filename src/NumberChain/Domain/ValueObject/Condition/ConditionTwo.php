<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\SimpleCondition;

final class ConditionTwo implements SimpleCondition
{
    public static function create(): self
    {
        return new self();
    }

    public function getResult(): int
    {
        return 1;
    }

    public function canBeApplied(int $number): bool
    {
        return 1 === $number;
    }
}

<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\SimpleCondition;

final class ConditionOne implements SimpleCondition
{
    public static function create(): self
    {
        return new self();
    }

    public function getResult(): int
    {
        return 0;
    }

    public function canBeApplied(int $number): bool
    {
        return 0 === $number;
    }
}

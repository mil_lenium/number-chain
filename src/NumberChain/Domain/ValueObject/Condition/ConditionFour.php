<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject\Condition;

use App\NumberChain\Domain\ValueObject\ComplexCondition;
use App\NumberChain\Domain\ValueObject\NumberCollector;

final class ConditionFour implements ComplexCondition
{
    public static function create(): self
    {
        return new self();
    }

    public function apply(int $number): NumberCollector
    {
        $number += -1;
        return NumberCollector::fromArray([$number / 2, $number / 2 + 1]);
    }

    public function canBeApplied(int $number): bool
    {
        if ($number < 3) {
            return false;
        }

        if (0 === $number % 2) {
            return false;
        }

        return true;
    }
}

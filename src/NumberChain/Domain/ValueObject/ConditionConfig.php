<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject;

use Exception;

final class ConditionConfig
{
    /**
     * @var ComplexCondition[]
     */
    private $complexConditions;

    /**
     * @var SimpleCondition[]
     */
    private $simpleConditions;

    public static function create(array $simpleConditions, array $complexConditions): self
    {
        self::validateComplexConditions($complexConditions);
        self::validateSimpleConditions($simpleConditions);
        return new self($complexConditions, $simpleConditions);
    }

    private function __construct(array $complexConditions, $simpleConditions)
    {
        $this->complexConditions = $complexConditions;
        $this->simpleConditions = $simpleConditions;
    }

    public function getComplexConditions(): array
    {
        return $this->complexConditions;
    }

    public function getSimpleConditions(): array
    {
        return $this->simpleConditions;
    }

    private static function validateSimpleConditions(array $conditions): void
    {
        foreach ($conditions as $condition) {
            self::validateCondition($condition, SimpleCondition::class);
        }
    }

    private static function validateComplexConditions(array $conditions): void
    {
        foreach ($conditions as $condition) {
            self::validateCondition($condition, ComplexCondition::class);
        }
    }

    private static function validateCondition(Condition $condition, string $class): void
    {
        if ($condition instanceof $class) {
            return;
        }

        $class = get_class($condition);
        throw new Exception("{$class} is not an instance of {$class}");
    }
}

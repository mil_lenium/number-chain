<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject;

final class NumberCollector
{
    /**
     * @var array
     */
    private $numbers;

    public static function fromArray(array $numbers): self
    {
        return new self($numbers);
    }

    private function __construct(array $numbers)
    {
        $this->numbers = $numbers;
    }

    public function toArray(): array
    {
        return $this->numbers;
    }
}

<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject;

interface Condition
{
    public function canBeApplied(int $number): bool;
}

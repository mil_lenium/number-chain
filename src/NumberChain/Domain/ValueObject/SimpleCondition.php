<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject;

interface SimpleCondition extends Condition
{
    public function getResult(): int;
}

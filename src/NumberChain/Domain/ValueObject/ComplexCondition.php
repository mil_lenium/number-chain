<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\ValueObject;

interface ComplexCondition extends Condition
{
    public function apply(int $number): NumberCollector;
}

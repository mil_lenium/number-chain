<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Service;

use Exception;

class DefaultChainCalculator implements ChainCalculator
{
    private const START_NUMBER = 0;
    private const MAX_NUMBER = 1000000;

    /**
     * @var NumberCalculator
     */
    private $numberCalculator;

    public function __construct(NumberCalculator $numberCalculator)
    {
        $this->numberCalculator = $numberCalculator;
    }

    public function calculate(array $numbers): array
    {
        $result = [];

        foreach ($numbers as $inputNumber) {
            $maxNumber = 0;
            $inputNumber = (int) $inputNumber;

            if (self::MAX_NUMBER < $inputNumber) {
                throw new Exception('Max number is ' . self::MAX_NUMBER);
            }

            for ($number = self::START_NUMBER; $number <= $inputNumber; $number++) {
                $calculatedValue = $this->numberCalculator->calculate($number);

                if ($maxNumber < $calculatedValue) {
                    $maxNumber = $calculatedValue;
                }
            }

            $result[$inputNumber] = $maxNumber;
        }

        return $result;
    }
}

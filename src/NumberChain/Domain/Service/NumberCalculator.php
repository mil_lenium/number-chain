<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Service;

interface NumberCalculator
{
    public function calculate(int $number): int;
}

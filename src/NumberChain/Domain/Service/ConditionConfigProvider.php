<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Service;

use App\NumberChain\Domain\ValueObject\ConditionConfig;

interface ConditionConfigProvider
{
    public function provide(): ConditionConfig;
}

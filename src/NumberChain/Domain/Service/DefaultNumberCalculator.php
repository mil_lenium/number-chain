<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Service;

use App\NumberChain\Domain\ValueObject\ComplexCondition;
use App\NumberChain\Domain\ValueObject\ConditionConfig;
use App\NumberChain\Domain\ValueObject\SimpleCondition;
use Exception;

class DefaultNumberCalculator implements NumberCalculator
{
    /**
     * @var ConditionConfig
     */
    private $conditions;

    /**
     * @var array
     */
    private $cache;

    public function __construct(ConditionConfig $conditions)
    {
        $this->conditions = $conditions;
        $this->cache = [];
    }

    public function calculate(int $number): int
    {
        if (isset($this->cache[$number])) {
            return $this->cache[$number];
        }

        /** @var SimpleCondition $condition */
        foreach ($this->conditions->getSimpleConditions() as $condition) {
            if (false === $condition->canBeApplied($number)) {
                continue;
            }

            return $condition->getResult();
        }

        return $this->applyComplexConditions($number);
    }

    private function applyComplexConditions(int $number): int
    {
        /** @var ComplexCondition $condition */
        foreach ($this->conditions->getComplexConditions() as $condition) {
            if (false === $condition->canBeApplied($number)) {
                continue;
            }

            $sum = 0;

            foreach ($condition->apply($number)->toArray() as $number) {
                $result = $this->calculate($number);
                $this->cache[$number] = $result;
                $sum += $result;
            }

            return $sum;
        }

        throw new Exception("Number {$number} cannot be calculated");
    }
}

<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Service;

use App\NumberChain\Domain\ValueObject\Condition\ConditionFour;
use App\NumberChain\Domain\ValueObject\Condition\ConditionOne;
use App\NumberChain\Domain\ValueObject\Condition\ConditionThree;
use App\NumberChain\Domain\ValueObject\Condition\ConditionTwo;
use App\NumberChain\Domain\ValueObject\ConditionConfig;

class DefaultConditionConfigProvider implements ConditionConfigProvider
{
    public function provide(): ConditionConfig
    {
        return ConditionConfig::create(
            [
                ConditionOne::create(),
                ConditionTwo::create()
            ],
            [
                ConditionThree::create(),
                ConditionFour::create()
            ]
        );
    }
}

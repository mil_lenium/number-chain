<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Service;

interface ChainCalculator
{
    public function calculate(array $numbers): array;
}

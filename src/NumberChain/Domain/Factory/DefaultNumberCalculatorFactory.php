<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Factory;

use App\NumberChain\Domain\Service\ConditionConfigProvider;
use App\NumberChain\Domain\Service\DefaultNumberCalculator;
use App\NumberChain\Domain\Service\NumberCalculator;

class DefaultNumberCalculatorFactory implements NumberCalculatorFactory
{

    /**
     * @var ConditionConfigProvider
     */
    private $conditionConfigProvider;

    public function __construct(ConditionConfigProvider $conditionConfigProvider)
    {
        $this->conditionConfigProvider = $conditionConfigProvider;
    }

    public function create(): NumberCalculator
    {
        return new DefaultNumberCalculator($this->conditionConfigProvider->provide());
    }
}

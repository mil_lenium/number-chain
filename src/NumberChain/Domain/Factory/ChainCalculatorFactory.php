<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Factory;

use App\NumberChain\Domain\Service\ChainCalculator;

interface ChainCalculatorFactory
{
    public function create(): ChainCalculator;
}

<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Factory;

use App\NumberChain\Domain\Service\NumberCalculator;

interface NumberCalculatorFactory
{
    public function create(): NumberCalculator;
}

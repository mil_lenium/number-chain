<?php

declare(strict_types=1);

namespace App\NumberChain\Domain\Factory;

use App\NumberChain\Domain\Service\ChainCalculator;
use App\NumberChain\Domain\Service\DefaultChainCalculator;

class DefaultChainCalculatorFactory implements ChainCalculatorFactory
{
    /**
     * @var NumberCalculatorFactory
     */
    private $numberCalculatorFactory;

    public function __construct(NumberCalculatorFactory $numberCalculatorFactory)
    {
        $this->numberCalculatorFactory = $numberCalculatorFactory;
    }

    public function create(): ChainCalculator
    {
        return new DefaultChainCalculator($this->numberCalculatorFactory->create());
    }
}
